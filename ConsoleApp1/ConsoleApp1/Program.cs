﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace El_Banco
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("---------------------------------------------------");

            Console.WriteLine(" Cuenta normal o regular ");
            Cuenta_Regular cuenta;
            cuenta = new Cuenta_Regular();
            cuenta.Saldo_regular = 400;
            cuenta.AbonarACuenta = 900;
            cuenta.RetirarDeCuenta = 320;
            cuenta.CalcularInterés = 1;
            cuenta.Credito = 900;
            float saldoneto, saldototal;
            saldoneto = cuenta.Saldo_regular + cuenta.AbonarACuenta;
            saldototal = saldoneto - cuenta.RetirarDeCuenta;
            Console.WriteLine(" El Saldo Actual es:" + cuenta.Saldo_regular + " pesos");
            Console.WriteLine(" El ingreso a su cuenta es:" + cuenta.AbonarACuenta + " pesos");
            Console.WriteLine(" Su Saldo Neto es:" + saldoneto + " pesos");
            Console.WriteLine(" Usted Retiro:" + cuenta.RetirarDeCuenta + " pesos");
            Console.WriteLine(" El saldo Total es:" + saldototal);
            Console.WriteLine(" El credito perdido es de:" + cuenta.Credito + " pesos");
            Console.WriteLine(" Su Interes por dejar pasar un dia es del: " + cuenta.CalcularInterés + "%" + " o 5 pesos");

            Console.WriteLine("_-------------------------------------------------");
            Console.WriteLine(" Cuenta dorada ");
            Cuenta_Dorada cuentadora;
            cuentadora = new Cuenta_Dorada();
            cuentadora.Saldo_dorado = 400;
            cuentadora.AbonarACuenta = 900;
            cuentadora.RetirarDeCuenta = 320;
            cuentadora.CalcularInterés = 10;
            cuentadora.Credito = 900;
            float saldoneto1, saldototal1;
            saldoneto1 = cuenta.Saldo_regular + cuenta.AbonarACuenta;
            saldototal1 = saldoneto1 - cuenta.RetirarDeCuenta;
            Console.WriteLine(" El Saldo Actual es:" + cuenta.Saldo_regular + " pesos");
            Console.WriteLine(" El ingreso a su cuenta es:" + cuenta.AbonarACuenta + " pesos");
            Console.WriteLine(" Su Saldo neto es de :" + saldoneto1 + " pesos");
            Console.WriteLine(" Su retiro fue de:" + cuenta.RetirarDeCuenta + " pesos");
            Console.WriteLine(" El saldo Total es:" + saldototal1);
            Console.WriteLine(" El credito perdido es de:" + cuenta.Credito + " pesos");
            Console.WriteLine(" Su Interes por dejar pasar un dia es del: " + "10%" + " o 50 pesos");
            Console.ReadKey();
        }
    }
}
