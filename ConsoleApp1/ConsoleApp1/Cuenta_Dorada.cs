﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace El_Banco
{
     public class Cuenta_Dorada : Clase_Ejecutiva
    {
        private float cat_interes;
        private float saldo_dorado;
        public float Cat_interes { get => cat_interes; set => cat_interes = value; }
        public float Saldo_dorado { get => saldo_dorado; set => saldo_dorado = value; }
    }
}
